Instalación

1)Instalar los requerimientos pip3 install -r requerimientos.txt

2)Descargar los archivos "settings.py" los cuales deben estar ubicados en la carpeta micomunidad 

3)Para cada aplicacion dentro de la carpeta "apps" deben crear una carpeta "migrations" y en ésta crear un archivo llamado __init__.py

4)Sincronizar la base de datos: python manage.py makemigrations, python manage.py migrate



Puntos importantes a considerar:

-- No subir el archivo settings.py al repositorio
-- Instalar los paquetes del archivo requirements.txt con el siguiente comando:
pip3 install -r requerimientos.txt

Tutorial para crear ambiente con python3 usando virtualenvwrapper

0)Se parte del supuesto que ya se tiene instalado virtualenvwrapper

1)Ejecutar: mkproject nombre <-- Esto crea un projecto y ambiente con el "nombre" que se de

2)Ejecutar: deactivate <-- Con esto salimos del ambiente

3)Ejecutar: rmvirtualenv nombreEnv  <-- Eliminaremos el ambiente que creamos con mkproject

4)Ejecutar: lsvirtualenv <-- lista los ambientes existente, asegurarse de que ya no aparezca el ambiente que eliminamos

5)Ejecutar: mkvirtualenv --python=/usr/bin/python3 nombreENV <-- Creamos un ambiente con python3

6)Ejecutar: lsvirtualenv <-- Nos aseguramos que el ambiente que creamos aparezca

7)Ejecutar: setvirtualenvproject <-- Asegurese que esta activado el ambiente que creo anteriormente y que se encuentra situado en la carpeta a la que le quiere asignar el ambiente

8)Ejecutar: python: Si el interprete muestra python3.5, entonces todo quedo perfecto.