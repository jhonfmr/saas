from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^confirmar$',verificacion_pago_evento, name='pago_evento_confirmar'),
    url(r'^realizar$',RegistroPago.as_view(), name='pago_evento_realizar'),
    url(r'^$', Inicio, name='inicio'),
]