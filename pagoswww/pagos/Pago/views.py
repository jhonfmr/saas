from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView
from django.contrib import messages

from .models import Pago
from .forms import PagoForm

def verificacion_pago_evento(request):
    cedula = request.GET['cedula']
    evento = request.GET['evento']
    empresa = request.GET['empresa']
    respuesta = {
        'pago':False
    }
    try:
        pago = Pago.objects.get(cedula = cedula, identificador_evento = evento,identificador_empresa = empresa)
        respuesta['pago'] = pago.esta_pago
    except ObjectDoesNotExist:
        pass

    return JsonResponse(respuesta)


class RegistroPago(CreateView):
    form_class = PagoForm
    template_name = "Pago/PagoForm.html"
    success_url = reverse_lazy('pago_evento_realizar')

    def get_context_data(self, **kwargs):
        context = super(RegistroPago, self).get_context_data(**kwargs)
        context['titulo'] = "Registro de pago"
        context['tipo'] = "Pagos"
        context['accion'] = "Registrar"
        context['pagos'] = Pago.objects.all()
        return context

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.esta_pago = True
        self.object.save()
        messages.success(self.request, "El pago se ha realizado satisfactoriamente")
        return super(RegistroPago, self).form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, "Error, no se registró el pago, por favor verificar los datos")
        return super(RegistroPago, self).form_invalid(form)


def Inicio(request):

    return render(request, 'Pago/inicio.html',{})