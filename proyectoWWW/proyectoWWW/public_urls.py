from django.conf.urls import url, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^', include('apps.vista_publica.urls')),
    url(r'^usuario/', include('apps.Usuario.urls')),
    url(r'^empresa/', include('apps.empresa.urls')),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)