from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.shortcuts import redirect


from datetimewidget.widgets import DateWidget


# Funcion que me retorna el selector de fecha
def MyDateWidget():
    return DateWidget(usel10n=False, bootstrap_version=3, options={'format': 'yyyy-mm-dd', 'startView':4, 'language':'es'})

# Funcion que me permite dejar solo la primera letra en mayuscula de ciertos campos de un formulario
# PARAMS:
# form_data = datos del formulario
# campos = listado de campos del formulario los cuales queremos convertir la primera letra en mayuscula
def capitalizar_texto_campos(form_data, campos):
    for campo in campos:
        if form_data.get(campo,None):
            form_data[campo] = form_data[campo].lower().capitalize()
    return form_data


# Decorador que verifica que el usuario tenga alguno de los cargos permitidos
def verificar_cargo(cargos_permitidos):
    def _method_wrapper(view_method):
        def _arguments_wrapper(request, *args, **kwargs):
            request_inicial = request
            if hasattr(request, "request"):  # Es una CBV
                request = request.request

            try:
                usuario = request.user
                if not (usuario.cargo in cargos_permitidos):
                    #messages.error(request,"Usted no tiene ninguno de los cargos permitidos para acceder a la página solicitada")
                    raise PermissionDenied
            except AttributeError:
                messages.error(request, "Para acceder a la página solicitada requiere loguearse")
                return redirect('inicio')

            return view_method(request_inicial, *args, **kwargs)

        return _arguments_wrapper

    return _method_wrapper

# Params: Diccionario con los datos descritos en: https://docs.djangoproject.com/en/1.11/topics/email/#emailmessage-objects
def enviar_email(request=None,**datos):
    from django.core.mail import EmailMessage
    mensaje_error = datos.get("mensaje_error","Error al enviar correo")
    try:
        email = EmailMessage(subject=datos["subject"], body=datos["body"], to=datos["to"])
        email.send()
    except OSError:
        if request:
            messages.error(request,mensaje_error)


def obtener_mensajes_respuesta(respuesta):
    mensajes = []
    if respuesta.context:
        mensajes_contexto = respuesta.context.get('messages', [])
        for mensaje in mensajes_contexto:
            mensajes.append(str(mensaje))

    if respuesta.wsgi_request._messages._queued_messages:
        mensajes_wsgi = respuesta.wsgi_request._messages._queued_messages

        for mensaje in mensajes_wsgi:
            mensajes.append(str(mensaje))
    return mensajes


def vigencia_disponible_empresa(function):
    def _inner(request, *args, **kwargs):
        from django.utils import timezone

        request_inicial = request
        if hasattr(request, "request"):  # Es una CBV
            request = request.request

        hoy = timezone.now()
        if request.tenant.schema_name != 'public':
            fin_vigencia = request.tenant.vigencia

            if hoy <= fin_vigencia:
                return function(request_inicial, *args, **kwargs)
            else:
                messages.error(request, "El servicio de la empresa %s se encuentra vencido, por favor contacte a su administrador"%request.tenant.nombre)
                return redirect('inicio')
        else:
            return function(request_inicial, *args, **kwargs)

    return _inner


# Decorador que verifica que el modulo este entre los incluidos en el plan de la empresa si es diferente al public
def verificar_modulo_incluido(modulo):
    def _method_wrapper(view_method):
        def _arguments_wrapper(request, *args, **kwargs):
            request_inicial = request
            if hasattr(request, "request"):  # Es una CBV
                request = request.request

            try:
                plan = request.tenant.plan
                if request.tenant.schema_name != "public":
                    if modulo not in plan.obtener_modulos_disponibles():
                        #messages.error(request,"El plan actual de la empresa no incluye la funcionalidad solicitada")
                        raise PermissionDenied
            except AttributeError:
                messages.error(request, "Para acceder a la página solicitada requiere loguearse")
                return redirect('inicio')

            return view_method(request_inicial, *args, **kwargs)

        return _arguments_wrapper

    return _method_wrapper