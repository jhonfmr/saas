from django.db import models
from django.contrib.auth.models import AbstractUser
from apps.Evento.models import Evento
from apps.Actividad.models import Actividad
from apps.Noticia.models import Noticia
from apps.Participante.models import ParticipantesEventos
from django.db.models.aggregates import Count, Sum
from simple_history.models import HistoricalRecords


def crear_ruta_imagen_perfil(instance, filename):
    return "imagenen-usuario/%s-%s" % (instance.id, filename.encode('ascii', 'ignore'))


class Usuario(AbstractUser):
    # opciones de cargo
    TIPOS_CARGO = (
        ('Administrador', 'Administrador'),
        ('Gerente', 'Gerente'),
        ('Operador', 'Operador'),
        ('Cliente', 'Cliente'),
        ('Participante', 'Participante'),
    )
    SEXO = (
        ('M', 'Masculino'),
        ('F', 'Femenino')
    )

    # datos basicos de un usuario
    cedula = models.PositiveIntegerField(verbose_name='Número de documento de identificación', unique=True)
    edad = models.PositiveIntegerField(verbose_name='edad')
    sexo = models.CharField(max_length=2,verbose_name='sexo',choices=SEXO)
    telefono = models.CharField(max_length=25, verbose_name='teléfono',blank=True,null=True)
    cargo = models.CharField(max_length=30,verbose_name='cargo',choices=TIPOS_CARGO)
    imagen = models.ImageField(upload_to=crear_ruta_imagen_perfil,verbose_name='imagen',blank=True)
    historia = HistoricalRecords(inherit=True)
    fecha_fin_membresia = models.DateField(verbose_name="fecha de finalización de la membresía", blank=True, null=True)

    # Permite crear un usuario con cargo Administrador solo si no hay usuarios registrados
    @staticmethod
    def crear_usuario_inicial():
        total_usuarios = Usuario.objects.all().count()
        if total_usuarios == 0:
            password = "admin123"
            user = Usuario.objects.create_user('admin', 'root@gmail.com', password, cedula=123, edad=30,
                                               telefono=5555555, )
            user.set_password(password)
            user.first_name = 'Administrador'
            user.is_superuser = True
            user.is_staff = True
            user.cargo = "Administrador"
            user.save()

    @staticmethod
    def crear_super_admin_cliente(cliente):
        user = Usuario.objects.create_user('admin', cliente.email, "%s-%s" % (cliente.cedula, cliente.first_name),
                                           cedula=cliente.cedula, edad=cliente.edad, telefono=cliente.telefono)
        user.first_name = 'Administrador'
        user.is_superuser = True
        user.is_staff = True
        user.cargo = 'Administrador'
        user.save()

    def es_administrador(self):
        if self.cargo == "Administrador":
            return True
        return False

    # Dependiendo del cargo del usuario, retornara su respectiva pagina
    def obtener_pagina_dashboard(self):
        if self.es_administrador():
            return "Usuario/dashboard_administrador.html"
        elif self.cargo == "Gerente":
            return "Usuario/dashboard_gerente.html"
        elif self.cargo == "Cliente":
            return "Usuario/dashboard_cliente.html"
        elif self.cargo == "Participante":
            return "Usuario/dashboard_participante.html"
        return "Usuario/dashboard_operador.html"

    def __str__(self):
        return self.get_full_name()

    # Dependiendo del cargo del usuario, retornara su respectivo contexto para el dashboard
    def datos_dashboard(self):
        cargo = self.cargo
        datos = {}

        if cargo == "Operador":
            datos['num_eventos'] = Evento.objects.all().count()
            datos['num_actividades'] = Actividad.objects.all().count()
            datos['num_noticias'] = Noticia.objects.all().count()
            datos['num_participantes'] = Usuario.objects.filter(cargo='Participante').count()
            datos['actividades'] = Actividad.objects.all()
        elif cargo == "Gerente":
            datos['num_eventos'] = Evento.objects.count()
            datos['num_actividades'] = Actividad.objects.count()
            datos['num_noticias'] = Noticia.objects.count()
            ingresos = ParticipantesEventos.objects.filter(estado_participante='INSCRITO').aggregate(
                Sum('evento__precio'))
            datos['ingresos'] = ingresos['evento__precio__sum']
            datos['actividades'] = Actividad.objects.all()
        elif cargo == "Cliente":
            from apps.empresa.models import Dominio
            datos['dominios'] = Dominio.objects.filter(tenant__cliente=self).select_related('tenant').all()
        else:  # cargo == "Administrador"
            datos['usuarios_totales'] = Usuario.objects.count()
            datos['usuarios_gerentes'] = Usuario.objects.filter(cargo='Gerente').count()
            datos['usuarios_operarios'] = Usuario.objects.filter(cargo='Operador').count()
            datos['usuarios_admin'] = Usuario.objects.filter(cargo='Administrador').count()

        return datos

    @staticmethod
    def obtener_participante(id_participante):
        try:
            return Usuario.objects.get(cargo='Participante', id=id_participante)
        except Exception:
            return None

    @staticmethod
    def buscar(id_usuario):
        try:
            return Usuario.objects.get(id=id_usuario)
        except Exception:
            return None

    @staticmethod
    def obtener_participante_sesion(request):
        id_participante = request.session.get('id_participante')
        if not id_participante:
            return None
        return Usuario.obtener_participante(id_participante)

    @staticmethod
    def buscar_participante_por_clave(clave):
        try:
            mi_id, mi_hash = map(int, clave.split("-"))
            aspirante = Usuario.objects.get(id=mi_id)
            if aspirante.numero_inscripcion() == clave:
                return aspirante
            else:
                return None
        except Exception as ex:
            return None


    def tiempo_membresia(self,tipo_membresia):
        import datetime
        from dateutil.relativedelta import relativedelta
        fecha_finalizacion = datetime.date.today() if not self.fecha_fin_membresia else self.fecha_fin_membresia
        membresia = {
            '1': fecha_finalizacion + relativedelta(months=+1), '2': fecha_finalizacion + relativedelta(months=+3),
            '3': fecha_finalizacion + relativedelta(months=+6), '4': fecha_finalizacion + relativedelta(years=+1)
        }
        return membresia[tipo_membresia]
