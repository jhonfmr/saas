from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^registro$', registro, name='participante_registro'),
    url(r'^preinscripcion$', preinscripcion, name ='preinscripcion_participante'),
    url(r'^listado', ListarParticipantesPre.as_view(), name='listado_participantes_pre'),
    url(r'^escarapela/(?P<id_participante>\d+)/(?P<id_evento>\d+)$', generarEscarapela, name='gen_escarapela_participante'),
    url(r'^modificar$', modificarParticipante,name = 'participante_modificar_participante'),
    url(r'^ingresar', ingresarDatosValidacion.as_view(),name = 'participante_ingresar_participante'),
    url(r'^membresia-participante/(?P<tipo_membresia>[1-4]{1})$',adquirir_membresia , name='participante_adquirir_membresia'),
]
