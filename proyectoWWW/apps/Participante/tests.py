from django.urls import reverse_lazy

from django_tenants.test.cases import TenantTestCase
from django_tenants.test.client import TenantClient
from django.utils import timezone

from proyectoWWW.utilidades import obtener_mensajes_respuesta
from .models import *
from apps.empresa.models import Plan
from apps.Usuario.models import Usuario
from apps.tema.models import Tema

class RegistrarPreinscripcionTestCase(TenantTestCase):


    def setup_tenant(self, tenant):
        from django.utils import timezone
        Plan.crear_planes_iniciales()
        Usuario.crear_usuario_inicial()
        tenant.plan = Plan.objects.all()[0]
        tenant.cliente = Usuario.objects.all()[0]
        tenant.vigencia = timezone.now() + timezone.timedelta(days=1)
        tenant.nombre = "Prueba"

    @staticmethod
    def get_test_tenant_domain():
        return 'localhost'

    @staticmethod
    def get_test_schema_name():
        return 'tester'

    def setUp(self):
        super(RegistrarPreinscripcionTestCase, self).setUp()
        self.cliente = TenantClient(self.tenant)
        Usuario.objects.all().delete()

        password = "participante123"
        user = Usuario.objects.create_user('participante', 'root@gmail.com', password, cedula = 123, edad = 30, telefono = 5555555, )
        user.set_password(password)
        user.first_name = 'cliente'
        user.is_superuser = True
        user.is_staff = True
        user.cargo = "Participante"
        user.save()
        tema = Tema.objects.create(nombre='Musica')
        tema.save()
        evento = Evento.objects.create(nombre='evento',ubicacion="No tiene",precio=1000,numero_participantes=50,
                              fecha_inicio=timezone.now(),fecha_fin=timezone.now())
        evento.temas.add(tema)
        evento.save()
        sesion = self.cliente.session
        sesion['id_evento'] = evento.pk
        sesion.save()

    def test_preinscripcion(self):
        self.cliente.login(username="participante", password='participante123')
        respuesta = self.cliente.post(reverse_lazy('preinscripcion_participante'))
        mensaje = obtener_mensajes_respuesta(respuesta)
        self.assertIn("El participante se ha pre-inscrito satisfactoriamente", mensaje)

    def test_preinscripcion_sin_login(self):
        respuesta = self.cliente.post(reverse_lazy('preinscripcion_participante'))
        mensaje = obtener_mensajes_respuesta(respuesta)
        self.assertIn("Para acceder a la página solicitada requiere loguearse", mensaje)
