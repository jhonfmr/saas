from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^ingresos-evento$', reporte_ingresos_evento,name = 'reporte_ingresos_evento_reporte'),
    url(r'^ingresos-evento-datos$', reporte_ingresos_evento_datos, name='reporte_ingresos_evento_reporte_datos'),
    url(r'^asistencia-evento$', reporte_asistencia_evento,name = 'reporte_asistencia_evento_reporte'),
    url(r'^asistencia-evento-datos$', reporte_asistencia_evento_datos,name = 'reporte_asistencia_evento_reporte_datos'),

    # ----------------- Participantes por evento -----------------
    url(r'^partcipantes-evento$', reporte_participantes_evento, name='reporte_participantes_evento_reporte'),
    url(r'^partcipantes-evento-datos$', reporte_participantes_evento_datos, name='reporte_participantes_evento_reporte_datos'),

    # --------------- Eventos por mes -------------------------
    url(r'^cantidad-evento$', reporte_cantidad_evento,name = 'reporte_cantidad_evento_reporte'),
    url(r'^cantidad-evento-datos$', reporte_cantidad_evento_datos,name = 'reporte_cantidad_evento_reporte_datos'),

    url(r'^asistencia_eventos_fecha$', porcentaje_participacion_fecha, name='reporte_porcentaje_participacion_fecha'),
    url(r'^asistencia_eventos_fecha_datos$', porcentaje_participacion_fecha_datos,
        name='reporte_porcentaje_participacion_fecha_datos'),
]
