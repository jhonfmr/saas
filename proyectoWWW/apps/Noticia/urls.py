from django.conf.urls import url
from apps.Noticia.views import *

urlpatterns = [
    url(r'^registro$', RegistroNoticia.as_view(), name ='noticia_registro'),
    url(r'^modificar/(?P<pk>\d+)$', ModificarNoticia.as_view(), name='modificar_noticia'),
    url(r'^eliminar/(?P<pk>\d+)$', EliminarNoticia.as_view(), name='eliminar_noticia'),
    url(r'^listado', ListarNoticias.as_view(), name='listado_noticias'),
    url(r'^detalle/(?P<id_noticia>\d+)$', detalle_noticia, name='noticia_detalle_noticia'),
]
