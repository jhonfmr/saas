from django import forms
from .models import *
from django_select2.forms import Select2MultipleWidget,Select2Widget

class NoticiaForm(forms.ModelForm):
    class Meta:
        model = Noticia
        exclude = ()
        widgets = {
            "descripcion": forms.Textarea(attrs={'rows': 5,'style':'resize:none;'}),
            "fecha": forms.DateInput(format='%Y-%m-%d'),
        }

    def clean_fecha(self):
        from datetime import date
        fecha = self.cleaned_data.get('fecha',None)
        fecha_actual = date.today()
        if fecha:
            if fecha > fecha_actual:
                raise forms.ValidationError("La fecha no puede ser futura")
        return fecha