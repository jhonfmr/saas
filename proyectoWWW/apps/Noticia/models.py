from django.db import models
from simple_history.models import HistoricalRecords

class Registro(models.Model):
    creado = models.DateTimeField(auto_now_add=True)
    modificado = models.DateTimeField(auto_now=True)
    historia = HistoricalRecords(inherit=True)

    class Meta:
        abstract = True

def crear_ruta_imagen_perfil(instance, filename):
    return "imagenes-noticias/%s-%s"%(instance.id, filename.encode('ascii','ignore'))


class Noticia(Registro):
    titulo = models.CharField(max_length=30,verbose_name='título de la noticia')
    descripcion = models.TextField(verbose_name='contenido de la noticia')
    imagen = models.ImageField(upload_to=crear_ruta_imagen_perfil,verbose_name='imagen')
    fecha = models.DateField(verbose_name='fecha')

    def __str__(self):
        return self.titulo

