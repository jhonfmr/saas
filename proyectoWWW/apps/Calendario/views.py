from django.http import JsonResponse
from django.shortcuts import render
from django.views.generic import TemplateView

from apps.Evento.models import Evento

from proyectoWWW.utilidades import verificar_cargo, verificar_modulo_incluido, vigencia_disponible_empresa


class VisualizarCalendario(TemplateView):
    template_name = 'Calendario/calendario.html'

    @verificar_modulo_incluido("Calendario")
    @verificar_cargo(cargos_permitidos=["Gerente","Operador"])
    @vigencia_disponible_empresa
    def dispatch(self, request, *args, **kwargs):
        return super(VisualizarCalendario, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(VisualizarCalendario, self).get_context_data(**kwargs)
        context['eventos'] = Evento.objects.all()
        return context

@verificar_modulo_incluido("Calendario")
@verificar_cargo(cargos_permitidos=["Gerente","Operador"])
@vigencia_disponible_empresa
def datos_calendario(request):
    import random

    color_aleatorio = lambda: random.randint(0, 255)

    pk_evento = request.GET['pk_evento']
    evento_seleccionado = Evento.objects.get(pk = pk_evento)
    actividades_evento = evento_seleccionado.actividades.all()


    lista = []
    for actividad in actividades_evento:
        event = {
            'titulo': actividad.nombre,
            'fechaInicio': "%s %s" %(actividad.fecha_inicio,actividad.hora_inicio),
            'FechaFin': "%s %s" %(actividad.fecha_fin,actividad.hora_fin),
            'color': '#%02X%02X%02X' % (color_aleatorio(), color_aleatorio(), color_aleatorio())
        }
        lista.append(event)
    datos_enviar = {
        'eventos': lista
    }
    return JsonResponse(datos_enviar)


