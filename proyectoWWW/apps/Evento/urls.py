from django.conf.urls import url
from .views import *

urlpatterns = [
    # Vistas de los eventos
    url(r'^registro$', RegistroEvento.as_view(),name = 'evento_registro_evento'),
    url(r'^listado$', ListarEventos.as_view(), name='evento_listado_evento'),
    url(r'^modificar/(?P<pk>\d+)$', ModificarEvento.as_view(), name='evento_modificar_evento'),
    url(r'^estado/(?P<id_evento>\d+)$', activar_desactivar_evento, name='evento_activar_desactivar_evento'),
    url(r'^detalle/(?P<id_evento>\d+)$', detalle_evento, name='evento_detalle_evento'),
    url(r'^dashboard/(?P<id_evento>\d+)$', dashboard_evento, name='evento_dashboard_evento'),
    url(r'^participantes/(?P<id_evento>\d+)$', ListarParticipantesEvento, name='evento_participante_evento'),

    url(r'^confirmar/(?P<id_participante>\d+)/(?P<id_evento>\d+)$', confirmar_participante,name = 'evento_confirmar_evento'),
    #url(r'^preinscripcion$', preinscripcion_intermedia,name = 'preinscripcion_intermedia'),
]
