from django.db import models

from apps.tema.models import *

from proyectoWWW.utilidades import enviar_email
from django_google_maps.fields import AddressField, GeoLocationField
from simple_history.models import HistoricalRecords

def crear_ruta_imagen_perfil(instance, filename):
    return "imagenes-eventos/%s-%s"%(instance.id, filename.encode('ascii','ignore'))


class Evento(Registro):
    ESTADOS = (
        ('ACTIVO', 'Activo'),
        ('INACTIVO', 'Inactivo'),
    )

    #datos basicos el evento
    nombre = models.CharField(max_length=80,verbose_name='nombre del evento',unique=True)
    descripcion = models.CharField(max_length=500,verbose_name='descripción',blank=True)
    ubicacion = models.CharField(max_length=150, verbose_name='ubicación')
    precio = models.PositiveIntegerField(verbose_name='precio del evento')
    estado = models.CharField(max_length=30, verbose_name='estado', default = 'ACTIVO', choices=ESTADOS)

    #limite en fechas del evento
    fecha_inicio = models.DateField(verbose_name='fecha de inicio')
    fecha_fin = models.DateField(verbose_name='fecha de finalización')

    imagen = models.ImageField(upload_to=crear_ruta_imagen_perfil,verbose_name='imagen',blank=True)
    numero_participantes = models.PositiveSmallIntegerField(verbose_name='número de participantes')
    #participantes = models.ManyToManyField(Participante)
    temas = models.ManyToManyField(Tema,related_name='eventos')

    address = AddressField(max_length = 100, verbose_name = "dirección", help_text = "Ingrese la dirección y será seleccionada en el mapa")
    geolocation = GeoLocationField(verbose_name = "geolocalización")


    class Meta:
        ordering = ['nombre']

    def __str__(self):
        return self.nombre

    @staticmethod
    def obtener_evento(id_evento):
        try:
            return Evento.objects.get(id=id_evento)
        except Exception:
            return None

    @staticmethod
    def obtener_evento_sesion(request):
        id_evento = request.session.get('id_evento')
        if not id_evento:
            return None
        return Evento.obtener_evento(id_evento)

    def enviar_correo_aceptacion(self,request, participante):
        mensaje = """
            Buen día Señor(a) %s %s, usted ha completado el proceso de inscripción al evento titulado %s.

        """ % (participante.first_name, participante.last_name, self.nombre)
        datos_email = {
            "subject": "[Evento] Aceptación de inscripción",
            "body": mensaje,
            "to": [participante.email],
            "mensaje_error": "Error al enviar correo de confirmación de inscripción, por favor notificar al participante",
        }
        enviar_email(request, **datos_email)

    def enviar_correo_preinscripcion(self,request, participante):
        tenant_schema_name = request.tenant.schema_name
        mensaje = """
            Buen día Señor(a) %s %s, usted ha completado el proceso de pre-inscripción al evento titulado %s.
            
            Para realizar el pago del evento debe tener presente la siguiente información:
            Su número de cédula: %s
            El identificador del evento: %s
            El identificador de la empresa: %s

        """ % (participante.first_name, participante.last_name,self.nombre, participante.cedula, self.id, tenant_schema_name)
        datos_email = {
            "subject": "[Evento] Preinscripción de evento",
            "body": mensaje,
            "to": [participante.email],
            "mensaje_error": "Error al enviar correo de confirmación de pre-inscripción, por favor notificar al participante",
        }
        enviar_email(request, **datos_email)


    def interoperabilidad_pagos(self,cedula_participante,empresa):
        from django.conf import settings
        import requests, json

        datos_enviar = {
            'cedula': cedula_participante,
            'evento': self.pk,
            'empresa': empresa
        }
        try:
            respuesta = requests.get(settings.URL_SERVICIO_PAGOS, params=datos_enviar, verify=False)
        except Exception:
            return 2

        if respuesta.status_code == requests.codes.ok:
            return 1 if respuesta.json()['pago'] else 0
        else:
            return 2

    @staticmethod
    def generar_anos():
        resultado = []
        anos = Evento.objects.dates('fecha_inicio','year',order='DESC')
        for ano in anos:
            resultado.append((ano.year,ano.year))
        return resultado
