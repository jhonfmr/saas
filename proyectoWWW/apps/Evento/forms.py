import datetime
from django import forms
from django_google_maps.widgets import GoogleMapsAddressWidget

from .models import *
from apps.Usuario.models import Usuario

from django_select2.forms import ModelSelect2MultipleWidget
from proyectoWWW.utilidades import capitalizar_texto_campos

class RegistroEventoForm(forms.ModelForm):
    class Meta:
        model = Evento
        exclude = ('estado',)
        widgets = {
            "fecha_inicio": forms.DateInput(format='%Y-%m-%d'),
            "fecha_fin": forms.DateInput(format='%Y-%m-%d'),
            "temas": ModelSelect2MultipleWidget(model=Tema,search_fields=['nombre__icontains'],),
            "descripcion": forms.Textarea(attrs={'rows': 5,'style':'resize:none;'}),
            "address": GoogleMapsAddressWidget,
        }


    def clean(self):
        form_data = self.cleaned_data
        capitalizar_texto_campos(form_data, ["nombre","descripcion","ubicacion"])
        fecha_ahora = datetime.date.today()
        fecha_inicio = form_data.get('fecha_inicio',None)
        fecha_fin = form_data.get('fecha_fin',None)
        if fecha_inicio != None and fecha_fin != None:
            if fecha_inicio < fecha_ahora:
                self._errors["fecha_inicio"] = ["La fecha de inicio no puede ser pasada"]
            if fecha_fin < fecha_inicio:
                self._errors["fecha_fin"] = ["La fecha de finalización debe ser mayor o igual a la fecha de inicio"]
            if fecha_fin < fecha_ahora:
                self._errors["fecha_fin"] = ["La fecha de finalización no puede ser pasada"]

        return form_data


class CedulaForm(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = ('cedula',)
        exclude = ()
