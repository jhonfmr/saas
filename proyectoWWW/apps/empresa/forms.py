from django import forms
from django_select2.forms import Select2MultipleWidget

from .models import *

class EmpresaForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        from django.conf import settings
        super(EmpresaForm, self).__init__(*args, **kwargs)
        self.fields['schema_name'].label = "Subdominio del sistema"
        self.fields['schema_name'].help_text = ("Esta será su direccion: midireccion%s")%(settings.DOMINIO)
        self.fields['cliente'].queryset = self.fields['cliente'].queryset.filter(cargo="Cliente")

    class Meta:
        model = Empresa
        exclude = ('tema','color')
        widgets = {
            'vigencia': forms.DateInput(attrs = {'class': 'component-date'},format='%Y-%m-%d'),
        }

    def clean(self):
        form_data = self.cleaned_data
        direccion_tenant = form_data["schema_name"]
        if direccion_tenant.lower() == 'www':
            self._errors['schema_name'] = ["No es posible registrar %s como dirección en el sistema"%direccion_tenant]

        try:
            Empresa.objects.get(schema_name=direccion_tenant)
            self._errors['schema_name'] = ["Ya existe una empresa con la dirección %s"%direccion_tenant]
        except:
            pass

        nombre_empresa = form_data["nombre"]
        try:
            Empresa.objects.get(nombre=nombre_empresa)
            self._errors['nombre'] = ["Ya existe una empresa con el nombre %s"%nombre_empresa]
        except:
            pass

class RegistrarEmpresaSolicitudForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        from django.conf import settings
        super(RegistrarEmpresaSolicitudForm, self).__init__(*args, **kwargs)
        self.fields['schema_name'].label = "Subdominio del sistema"
        self.fields['schema_name'].help_text = ("Esta será su direccion: midireccion%s")%(settings.DOMINIO)

    class Meta:
        model = Empresa
        exclude = ('cliente','plan')
        widgets = {
            'vigencia': forms.DateInput(attrs = {'class': 'component-date'},format='%Y-%m-%d'),
        }

    def clean(self):
        form_data = self.cleaned_data
        direccion_tenant = form_data["schema_name"]
        if direccion_tenant.lower() == 'www':
            self._errors['schema_name'] = ["No es posible registrar %s como dirección en el sistema"%direccion_tenant]

        try:
            Empresa.objects.get(schema_name=direccion_tenant)
            self._errors['schema_name'] = ["Ya existe una empresa con la dirección %s"%direccion_tenant]
        except:
            pass

        nombre_empresa = form_data["nombre"]
        try:
            Empresa.objects.get(nombre=nombre_empresa)
            self._errors['nombre'] = ["Ya existe una empresa con el nombre %s"%nombre_empresa]
        except:
            pass

class ModificarEmpresaForm(forms.ModelForm):

    class Meta:
        model = Empresa
        exclude = ('schema_name','cliente',)
        widgets = {
            'vigencia': forms.DateInput(attrs = {'class': 'component-date'},format='%Y-%m-%d'),
        }

    def clean(self):
        form_data = self.cleaned_data
        nombre_empresa = form_data["nombre"]
        try:
            Empresa.objects.get(nombre=nombre_empresa).exclude(id=self.instance.id)
            self._errors['nombre'] = ["Ya existe una empresa con el nombre %s"%nombre_empresa]
        except:
            pass

class SolicitudEmpresaForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        from django.conf import settings
        super(SolicitudEmpresaForm, self).__init__(*args, **kwargs)
        self.fields['schema_name'].label = "Dirección web"
        self.fields['schema_name'].help_text = ("Esta será su direccion: midireccion%s")%(settings.DOMINIO)

    class Meta:
        model = SolicitudEmpresa
        fields = ('nombre', 'schema_name','tema','color','vigencia')
        widgets = {
            'vigencia': forms.DateInput(attrs = {'class': 'component-date'},format='%Y-%m-%d'),
        }

    def clean(self):
        form_data = self.cleaned_data
        direccion_tenant = form_data.get("schema_name","")
        if direccion_tenant.lower() == 'www':
            self._errors['schema_name'] = ["No es posible registrar %s como dirección en el sistema"%direccion_tenant]

        try:
            Empresa.objects.get(schema_name=direccion_tenant)
            self._errors['schema_name'] = ["Ya existe una empresa con la dirección %s"%direccion_tenant]
        except:
            pass

        nombre_empresa = form_data.get("nombre","")
        try:
            Empresa.objects.get(nombre=nombre_empresa)
            self._errors['nombre'] = ["Ya existe una empresa con el nombre %s"%nombre_empresa]
        except:
            pass


class ModificarEmpresaClienteForm(forms.ModelForm):

    class Meta:
        model = Empresa
        exclude = ('schema_name','plan','cliente')
        widgets ={
            'vigencia': forms.DateInput(attrs = {'class': 'component-date'},format='%Y-%m-%d'),
        }
    def clean(self):
        form_data = self.cleaned_data
        nombre_empresa = form_data["nombre"]
        try:
            Empresa.objects.exclude(id = self.instance.id).get(nombre=nombre_empresa)
            self._errors['nombre'] = ["Ya existe una empresa con el nombre %s"%nombre_empresa]
        except:
            pass


class PlanForm(forms.ModelForm):

    class Meta:
        model = Plan
        fields = ('nombre', 'precio', 'modulos_disponibles', 'maximo_usuarios')

        widgets = {
            'modulos_disponibles': Select2MultipleWidget()
        }