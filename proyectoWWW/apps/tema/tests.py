from django.urls import reverse_lazy
from django_tenants.test.cases import FastTenantTestCase,TenantTestCase
from django_tenants.test.client import TenantClient

from proyectoWWW.utilidades import obtener_mensajes_respuesta
from .models import *

from apps.Usuario.models import Usuario
from apps.empresa.models import Plan


class GestionTemaTestCase(TenantTestCase):
    @staticmethod
    def get_test_tenant_domain():
        return 'localhost'

    @staticmethod
    def get_test_schema_name():
        return 'tester'

    def setup_tenant(self, tenant):
        from django.utils import timezone
        Plan.crear_planes_iniciales()
        Usuario.crear_usuario_inicial()
        tenant.plan = Plan.objects.all()[0]
        tenant.cliente = Usuario.objects.all()[0]
        tenant.vigencia = timezone.now() + timezone.timedelta(days=1)
        tenant.nombre = "Prueba"

    def setUp(self):
        super(GestionTemaTestCase, self).setUp()
        self.cliente = TenantClient(self.tenant)

        Usuario.objects.all().delete()

        password = "operador123"
        user = Usuario.objects.create_user('operador', 'root@gmail.com', password, cedula=123, edad=30,
                                           telefono=5555555, )
        user.set_password(password)
        user.first_name = 'operador'
        user.is_superuser = True
        user.is_staff = True
        user.cargo = "Operador"
        user.save()

        Tema.objects.all().delete()
        tema = Tema.objects.create(nombre='Musica')
        tema.save()

    def test_registro_correcto_tema(self):
        self.cliente.login(username="operador", password='operador123')
        respuesta = self.cliente.post(reverse_lazy('tema_registro_tema'),
                                      {
                                          'nombre': 'Arte'
                                      })
        mensaje = obtener_mensajes_respuesta(respuesta)
        self.assertIn("Se ha registrado exitosamente el tema", mensaje)

    def test_registro_tema_sin_login(self):
        respuesta = self.cliente.post(reverse_lazy('tema_registro_tema'),
                                      {
                                          'nombre': 'Musica'
                                      })
        mensaje = obtener_mensajes_respuesta(respuesta)
        self.assertIn("Para acceder a la página solicitada requiere loguearse", mensaje)


    def test_registro_campo_vacio(self):
        self.cliente.login(username="operador", password='operador123')
        respuesta = self.cliente.post(reverse_lazy('tema_registro_tema'),
                                      {
                                          'nombre': ''
                                      })
        mensajes = obtener_mensajes_respuesta(respuesta)
        self.assertIn("Error, no se registró el tema, por favor verificar los datos", mensajes)

    def test_modificar_tema(self):
        self.cliente.login(username="operador", password='operador123')
        respuesta = self.cliente.post(reverse_lazy('tema_modificar_tema', kwargs={'pk': 1}),
                                      {
                                          'nombre': 'Salsa'
                                      })
        mensaje = obtener_mensajes_respuesta(respuesta)
        self.assertIn("Se ha modificado exitosamente el tema", mensaje)

    def test_eliminar_tema(self):
        self.cliente.login(username="operador", password='operador123')
        respuesta = self.cliente.get(reverse_lazy('tema_eliminar_tema', kwargs={'id_tema': 1}))
        mensaje = obtener_mensajes_respuesta(respuesta)
        self.assertIn("Se ha eliminado exitosamente el tema", mensaje)