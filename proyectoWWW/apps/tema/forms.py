from django import forms

from .models import *


class TemaForm(forms.ModelForm):
    class Meta:
        model = Tema
        exclude = ()

    def clean(self):
        from django.db.models import Q
        form_data = self.cleaned_data
        try:
            tema = Tema.objects.get(~Q(id=self.instance.id), nombre=form_data.get('nombre'))
            self._errors['nombre'] = ["Ya existe un tema con el nombre %s" % tema.nombre]
        except Tema.DoesNotExist:
            pass

        return form_data

    def save(self, commit=True):
        instancia = super(TemaForm, self).save(commit=False)
        instancia.nombre = instancia.nombre.capitalize()
        if commit:
            instancia.save()
        return instancia
