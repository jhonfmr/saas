from __future__ import absolute_import
from django import forms
from django_select2.forms import Select2Widget
from apps.Actividad.models import Actividad
from apps.Evento.models import Evento
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render, redirect

class ActividadForm(forms.ModelForm):
    class Meta:
        model = Actividad
        exclude = ('estado',)

        widgets = {
            'hora_inicio': forms.TimeInput(attrs={'class':'component-time'}),
            'hora_fin': forms.TimeInput(attrs={'class':'component-time'}),
            'fecha_inicio': forms.DateInput(format='%Y-%m-%d'),
            'fecha_fin': forms.DateInput(format='%Y-%m-%d'),
            'evento': Select2Widget(),
        }

    def clean(self):
        form_data = self.cleaned_data
        nombre=form_data['evento']
        fecha_inicio_actividad = form_data.get('fecha_inicio',None)
        fecha_fin_actividad = form_data.get('fecha_fin',None)
        
        evento = Evento.objects.get(nombre=nombre)
        if fecha_inicio_actividad != None and fecha_fin_actividad != None:
            if not(fecha_inicio_actividad >= evento.fecha_inicio and fecha_inicio_actividad <= evento.fecha_fin):
                self._errors["fecha_inicio"]=["La fecha de inicio debe estar entre la fecha de inicio y fin del evento"]
            elif not(fecha_fin_actividad <= evento.fecha_fin and fecha_fin_actividad >= evento.fecha_inicio):
                self._errors["fecha_fin"]=["La fecha de fin debe estar entre la fecha de inicio y fin del evento"]
            elif not(fecha_fin_actividad >= fecha_inicio_actividad):
                self._errors["fecha_fin"] = ["La fecha de finalización debe ser mayor o igual a la fecha de inicio"]

        return form_data