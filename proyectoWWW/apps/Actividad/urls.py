from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^listar$', ActividadList.as_view(), name='actividad_listar'),
    url(r'^crear$', ActividadCreate.as_view(), name='actividad_crear'),
    url(r'^modificar/(?P<pk>\d+)$', ActividadModificar.as_view(), name='actividad_modificar'),
    url(r'^estado/(?P<id_actividad>\d+)$', ActividadDesactivar, name='actividad_desactivar'),
    url(r'^obtener-evento$', obtener_evento, name='actividad_obtener_evento'),
]
