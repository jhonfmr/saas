from django.db import models
from apps.Evento.models import Evento
from simple_history.models import HistoricalRecords

class Registro(models.Model):
    creado = models.DateTimeField(auto_now_add=True)
    modificado = models.DateTimeField(auto_now=True)
    historia = HistoricalRecords(inherit=True)

    class Meta:
        abstract = True

class Actividad(Registro):
    ESTADOS = (
        ('ACTIVO', 'Activo'),
        ('INACTIVO', 'Inactivo'),
    )

    nombre = models.CharField(max_length=30,verbose_name='nombre de la actividad')
    fecha_inicio = models.DateField(verbose_name='fecha de inicio de la actividad')
    fecha_fin = models.DateField(verbose_name='fecha de finalizacion de la actividad')
    hora_inicio = models.TimeField(verbose_name='hora de inicio')
    hora_fin = models.TimeField(verbose_name='hora de finalización')
    ubicacion = models.CharField(max_length=150,verbose_name='ubicacion')
    estado = models.CharField(max_length=30, verbose_name='estado', default='ACTIVO', choices=ESTADOS)
    evento = models.ForeignKey(Evento,related_name='actividades')

    def __str__(self):
        return self.nombre