from django.urls import reverse_lazy

from django_tenants.test.cases import TenantTestCase
from django_tenants.test.client import TenantClient
from django.utils import timezone
from datetime import date, time

from .models import *
from apps.empresa.models import Plan
from apps.Usuario.models import Usuario
from apps.tema.models import Tema
from proyectoWWW.utilidades import obtener_mensajes_respuesta


class RegistrarActividadTestCase(TenantTestCase):

    def setup_tenant(self, tenant):
        from django.utils import timezone
        Plan.crear_planes_iniciales()
        Usuario.crear_usuario_inicial()
        tenant.plan = Plan.objects.all()[0]
        tenant.cliente = Usuario.objects.all()[0]
        tenant.vigencia = timezone.now() + timezone.timedelta(days=1)
        tenant.nombre = "Prueba"

    @staticmethod
    def get_test_tenant_domain():
        return 'localhost'

    @staticmethod
    def get_test_schema_name():
        return 'tester'

    def setUp(self):
        super(RegistrarActividadTestCase, self).setUp()
        self.cliente = TenantClient(self.tenant)
        Usuario.objects.all().delete()

        password = "operador123"
        user = Usuario.objects.create_user('operador', 'root@gmail.com', password, cedula=123, edad=30,
                                           telefono=5555555, )
        user.set_password(password)
        user.first_name = 'cliente'
        user.is_superuser = False
        user.is_staff = False
        user.cargo = "Operador"
        user.save()

        password = "gerente123"
        gerente = Usuario.objects.create_user('gerente', 'root@gmail.com', password, cedula=456, edad=30,
                                              telefono=5555555, )
        gerente.set_password(password)
        gerente.first_name = 'gerente'
        gerente.is_superuser = False
        gerente.is_staff = False
        gerente.cargo = "Gerente"
        gerente.save()

        tema = Tema.objects.create(nombre='Musica')
        tema.save()

        evento = Evento.objects.create(nombre='Evento prueba', ubicacion='Univalle',
                                       precio=1000, fecha_inicio=date(2018, 7, 4), fecha_fin=date(2018, 7, 23),
                                       numero_participantes=20, address="Univalle", geolocation="45,45")
        evento.temas.add(tema)
        evento.save()

        actividad = Actividad(nombre='Actividad de prueba', fecha_inicio=date(2018, 7, 5),
                              fecha_fin=date(2018, 7, 8),
                              hora_inicio=timezone.now(), hora_fin=timezone.now(), ubicacion='331')
        actividad.evento = evento
        actividad.save()

    def test_registro_actividad(self):
        self.cliente.login(username="operador", password='operador123')
        respuesta = self.cliente.post(reverse_lazy('actividad_crear'),
                                      {'nombre': "Actividad", 'ubicacion': "Univalle",
                                       'fecha_inicio': date(2018, 7, 10), 'fecha_fin': date(2018, 7, 17),
                                       'hora_inicio': time(10, 30, 0), 'hora_fin': time(20, 0, 0), 'evento': ['1']})
        mensaje = obtener_mensajes_respuesta(respuesta)
        self.assertIn("La actividad se ha guardado satisfactoriamente", mensaje)

    def test_registro_actividad_sin_login(self):
        respuesta = self.cliente.post(reverse_lazy('actividad_crear'),
                                      {'nombre': "Actividad", 'ubicacion': "Univalle",
                                       'fecha_inicio': date(2018, 7, 10), 'fecha_fin': date(2018, 7, 17),
                                       'hora_inicio': time(10, 30, 0), 'hora_fin': time(20, 0, 0), 'evento': ['1']})
        mensajes = obtener_mensajes_respuesta(respuesta)
        self.assertIn("Para acceder a la página solicitada requiere loguearse", mensajes)

    def test_registro_actividad_sin_nombre(self):
        self.cliente.login(username="operador", password='operador123')
        respuesta = self.cliente.post(reverse_lazy('actividad_crear'),
                                      {'nombre': "", 'ubicacion': "Univalle",
                                       'fecha_inicio': date(2018, 7, 10), 'fecha_fin': date(2018, 7, 17),
                                       'hora_inicio': time(10, 30, 0), 'hora_fin': time(20, 0, 0), 'evento': ['1']})

        mensajes = obtener_mensajes_respuesta(respuesta)
        self.assertIn("Error, no se registró la actividad, por favor verificar los datos", mensajes)


